<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Schema\ForeignKeyDefinition;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estabelecimentos', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('nome', 70);
            $table->string('rua', 70);
            $table->string('bairro', 70);
            $table->string('numero');
            $table->text('descricao');
            $table->string('cidade', 70);
            $table->string('estado', 2);
            $table->string('telefone', 20);
            $table->string('celular', 20);
            $table->foreignId('user_id')->constrained();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estabelecimento');
    }
};
