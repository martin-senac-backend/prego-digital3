<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nome');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('cpf', 20);
            $table->string('cep', 20);
            $table->string('rua', 70);
            $table->string('bairro', 70);
            $table->string('numero');
            $table->string('tipo', 45)->default('logista');
            $table->string('cidade', 70);
            $table->string('estado', 2);
            $table->string('password');
            $table->string('telefone', 14);
            $table->string('celular', 20);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
