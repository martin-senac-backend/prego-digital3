<?php

use App\Http\Controllers\CadastrarController;
use App\Http\Controllers\Admin\UsuariosController;
use App\Http\Controllers\Admin\ComprasController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home'])->name('home');

Route::get('/sobre', [HomeController::class, 'sobre'])->name('sobre');

Route::get('/contate', [HomeController::class, 'contate'])->name('contate');

Route::get('/login', [LoginController::class, 'login'])->name('login');

Route::get('/cadastrar', [CadastrarController::class, 'cadastrar'])->name('cadastrar');

Route::post('/cadastrar', [CadastrarController::class, 'salvar'])->name('salvar');



Route::prefix('/admin')->group(function () {

    Route::get('/', [AdminController::class, 'home'])
        ->name('admin.home');

    Route::get('/usuarios', [UsuariosController::class, 'index'])
        ->name('admin.usuarios.index');

    Route::get('/usuarios/cadastrar', [UsuariosController::class, 'create'])
        ->name('admin.usuarios.cadastrar');

    Route::post('/usuarios/cadastrar', [UsuariosController::class, 'store'])
        ->name('admin.usuarios.cadastrar');

    Route::get('/usuarios/editar/{id}', [UsuariosController::class, 'edit'])
        ->name('admin.usuarios.editar');

    Route::put('/usuarios/editar/{id}', [UsuariosController::class, 'update'])
        ->name('admin.usuarios.editar');

    Route::delete('/usuarios/deletar/{id}', [UsuariosController::class, 'destroy'])
        ->name('admin.usuarios.deletar');



    Route::get('/compras', [ComprasController::class, 'index'])
        ->name('admin.compras.index');

    Route::get('/compras/cadastrar', [ComprasController::class, 'create'])
        ->name('admin.compras.cadastrar');

    Route::post('/compras/cadastrar', [ComprasController::class, 'store'])
        ->name('admin.compras.cadastrar');

    Route::get('/compras/editar/{id}', [ComprasController::class, 'edit'])
        ->name('admin.compras.editar');

    Route::put('/compras/editar/{id}', [ComprasController::class, 'update'])
        ->name('admin.compras.editar');

    Route::delete('/compras/deletar/{id}', [ComprasController::class, 'destroy'])
        ->name('admin.compras.deletar');
});
