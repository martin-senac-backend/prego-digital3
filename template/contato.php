<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet"  href="assets/css/contato.css">
    <link rel="icon" type="image/png" href="assets/img/logoPD.ico" >
    <title>Formulário de Cadastro</title>
</head>
<body>

<header class="container fixed-top">

<nav class="navbar navbar-expand-lg" style="background-color: #6959CD;">
<div class="container-fluid">
                <a class="navbar-brand" href="index.php">
                    <img src="assets/img/logoPD.png" width="45" height="45" class="d-inline-block align-top" alt="">
                    <strong class="text-brand">Prego Digital</strong>
                </a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarText">

                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="sobre.php">Sobre Nós</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contato.php">Contate-Nos</a>
                        </li>

                    </ul>

                    <span class="sessao-usuario">
                        <a class="btn" style="background-color: #836FFF;" href="cadastro.php">Crie uma conta</a>
                        <a class="btn" style="background-color: #836FFF;" href="login.php">Entrar</a>
                    </span>


                </div>
            </div>

            
</nav>
<div id="main-container">
      <div id="address-container">
         <div class="fade"></div>
         <div id="address-content">
            <h2><ion-icon name="call-outline"></ion-icon>Telefone</h2>
            <p>(00) 00000-0000</p>
            <h2><ion-icon name="mail-outline"></ion-icon>E-Mail</h2>
            <p>name@example.com</p>
         </div>
      </div>

      <div id="form-container">
         <h2>Nos mande uma mensagem!</h2>
         <form id="contact-form">
            <label for="name">Nome:</label>
            <input type="text" name="name" placeholder="Digite seu nome">
            <label for="email">Email:</label>
            <input type="email" name="email" placeholder="Digite seu email">
            <label for="phone">Telefone:</label>
            <input type="text" name="phone" placeholder="Digite seu telefone">
            <label for="msg">Mensagem:</label>
            <textarea name="msg" placeholder="Como podemos te ajudar?"></textarea>
            <input type="submit" value="Enviar Mensagem">
         </form>
      </div>
   </div>

   <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
   <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>

</header>
    
</body>
</html>