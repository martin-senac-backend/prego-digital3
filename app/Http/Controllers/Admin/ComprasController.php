<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Compra;
use Exception;
use Illuminate\Http\Request;

class ComprasController extends Controller
{
    public function index()
    {
        $compras = Compra::all();
        //dd($usuarios);
        return view('admin.compras.index', [
            'compras' => $compras,
            'meuNome' => "Martin Almeida"
        ]);
    }

    public function create()
    {
        return view('admin.compras.cadastrar', ['compra' => new Compra]);
    }


    public function store(Request $request)
    {
        //dd($request);

        $request->validate([
            'titulo' => 'required',
            'descricao' => 'required',
            'valor' => 'required',
            'datacompra' => 'required',

        ]);

        try {
            $compra = new Compra();

            $compra->titulo = $request->titulo;
            $compra->descricao = $request->descricao;
            $compra->valor = $request->valor;
            $compra->datacompra = $request->datacompra;
            $compra->user_id = $request->user_id;
            $compra->save();

            return redirect()->route('admin.compras.index')->with('sucesso', 'Compra efetuada com sucesso');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao cadastrar, por favor tente novamente');
        }
    }


    public function edit($id)
    {
        return view('admin.compras.editar', [
            'compra' => Compra::findOrFail($id)
        ]);
    }


    public function update(Request $request, $id)
    {
        //dd($request);

        $request->validate([
            'nome' => 'required',
            'email' => "required|email|unique:users,id,{$id}",
            'password' => 'sometimes|nullable|min:8'
        ]);

        try {
            $compra = new Compra();
            $compra->id = $request->id;
            $compra->titulo = $request->titulo;
            $compra->descricao = $request->descricao;
            $compra->valor = $request->valor;
            $compra->datacompra = $request->datacompra;
            $compra->user_id = $request->user_id;

            if (!empty($request->password)) {
                $compra->password = bcrypt($request->password);
            }

            $compra->save();

            return redirect()->route('admin.compras.index')->with('sucesso', 'Usuario editado com sucesso');
        } catch (\Exception $e) {

            //dd($e->getMessage());

            return redirect()->back()->withInput()->with('erro', 'Ocorreu um erro ao editar, por favor tente novamente');
        }
    }



    public function destroy($id)
    {
        try {
            Compra::findOrFail($id)->delete();

            return redirect()->route('admin.compras.index')->with('sucesso', 'Usuario deletado com sucesso');
        } catch (Exception $e) {

            return redirect()->back()->with('erro', 'Ocorreu um erro ao editar, por favor tente novamente');
        }
    }
}
