<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('site.home');
    }

    public function sobre()
    {
        return view('site.sobre');
    }

    public function contate()
    {
        return view('site.contate');
    }
}
