<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    public function getUsuarios()
    {
        $usuarios = User::all();

        return $usuarios->toJson();
    }

    public function cadastrarUsuario(Request $request)
    {
    }
}
