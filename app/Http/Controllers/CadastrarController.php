<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class CadastrarController extends Controller
{
    public function cadastrar()
    {
        return view('login.cadastro');
    }

    public function salvar(Request $request)
    {

        //dd($request);
        $request->validate([
            'nome' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'cpf' => 'required',
            'cep' => 'required'
        ]);

        try {
            $usuario = new User();
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->password = bcrypt($request->password);
            $usuario->cpf = $request->cpf;
            $usuario->bairro = $request->bairro;
            $usuario->rua = $request->rua;
            $usuario->numero = $request->numero;
            $usuario->cidade = $request->cidade;
            $usuario->estado = $request->estado;
            $usuario->cep = $request->cep;
            $usuario->telefone = $request->telefone;
            $usuario->celular = $request->celular;
            $usuario->save();
            return view('login.sucesso');
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
    }
}
