@extends('layouts.site')

@section('titulo', 'sobre')

@section('conteudo')

<div class="container marketing" style="margin-top: 6%;">

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 text-center">
            <h2 class="texto-branco">Nossa <span class="texto-branco">Missão</span></h2>
            <p class="lead">Temos como objetivo organizar e manter o registro para melhor controle do estabelecimento e do cliente promovendo para que ambos tenham um excelente relacionamento de compra e venda.</p>
        </div>
        <div class="col-md-5">
            <img src="/img/missao.png" alt="" width="200">
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 order-md-2 text-center">
            <h2 class="texto-branco">Nossa <span class="texto-branco">Visão</span></h2>
            <p class="lead">Ser um site inovador que oferece a melhor solução de controle e gestão de contas a receber para os pequenos e grandes estabelecimentos do Brasil.</p>
        </div>
        <div class="col-md-5 order-md-1">
            <div class="col-md-5">
                <img src="/img/visao.png" alt="" width="200">
            </div>
        </div>
    </div>

    <hr class="featurette-divider">

    <div class="row featurette">
        <div class="col-md-7 text-center">
            <h2 class="texto-branco">Nossos <span class="texto-branco">Valores</span></h2>
            <p class="lead">Obstinação por resultados</p>
            <p class="lead">Inovação</p>
            <p class="lead">Integridade</p>
            <p class="lead">Resiliência</p>
            <p class="lead">Excelência</p>
        </div>
        <div class="col-md-5">
            <div class="col-md-5">
                <img src="/img/valores.png" alt="" width="200">
            </div>
        </div>
    </div>

    <hr class="featurette-divider">


</div>

@endsection
