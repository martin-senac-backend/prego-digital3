@extends('layouts.site')

@section('titulo', 'contate')

@section('conteudo')

<link rel="stylesheet" href="/css/contate.css">
<br>
<br>
<div id="main-container">
    <div id="address-container">
       <div class="fade"></div>
       <div id="address-content">
          <h2><ion-icon name="call-outline"></ion-icon>Telefone</h2>
          <p>(00) 00000-0000</p>
          <h2><ion-icon name="mail-outline"></ion-icon>E-Mail</h2>
          <p>name@example.com</p>
       </div>
    </div>

    <div id="form-container">
       <h2>Nos mande uma mensagem!</h2>
       <form id="contact-form">
          <label for="name">Nome:</label>
          <input type="text" name="name" placeholder="Digite seu nome">
          <label for="email">Email:</label>
          <input type="email" name="email" placeholder="Digite seu email">
          <label for="phone">Telefone:</label>
          <input type="text" name="phone" placeholder="Digite seu telefone">
          <label for="msg">Mensagem:</label>
          <textarea name="msg" placeholder="Como podemos te ajudar?"></textarea>
          <input type="submit" value="Enviar Mensagem">
       </form>
    </div>
 </div>

@endsection
