@extends('layouts.site')

@section('titulo', 'Home')

@section('conteudo')
<div class="container col-xxl-8 px-4 py-5">
    <div class="row flex-lg-row-reverse align-items-center g-5 py-5">
        <div class="col-10 col-sm-8 col-lg-6">
            <img src="/img/logoPD.png" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
        </div>
        <div class="col-lg-6">
            <h1 class="display-5 fw-bold lh-1 mb-3">Seja bem vindo ao    Prego Digital</h1>
            <p class="lead">O Prego Digital surgiu por meio da necessidade de organizar e rasgar de vez as famosas anotações em papeis de embalagens de pão ou em cadernetas, onde muitas vezes se perde ou estraga por diversos motivos deixando o proprietário do estabelecimento com o prejuízo da dívida não paga pelo seu devedor.</p>
            <a href="{{route('cadastrar')}}">
            <div class="d-grid gap-2 d-md-flex justify-content-md-start">
                <button type="button" style="background-color: #836FFF;" class="btn btn-lg px-4">Faça sua conta e controle seu estabelecimento</button>
            </a>
            </div>
        </div>
    </div>
</div>
@endsection
