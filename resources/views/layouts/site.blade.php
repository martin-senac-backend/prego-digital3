<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title @yield('titulo')>Prego Digital</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/cadastro.css" />
    <link rel="icon" type="image/png" href="/img/logoPD.ico">
</head>

<body>
    <header class="container fixed-top">

        <nav class="navbar navbar-expand-lg" style="">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{route('home')}}">
                    <img src="/img/logoPD.png" width="45" height="45" class="d-inline-block align-top" alt="">
                    <strong class="text-brand">Prego Digital</strong>
                </a>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarText">

                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('home')}}">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('sobre')}}">Sobre Nós</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('contate')}}">Contate-Nos</a>
                        </li>

                    </ul>

                    <span class="sessao-usuario">
                        <a class="btn" style="background-color:  #836FFF;" href="{{route('cadastrar')}}">Crie uma conta</a>
                        <a class="btn" style="background-color:  #836FFF;" href="{{route('login')}}">Entrar</a>
                    </span>


                </div>
            </div>
        </nav>

        <br>
    </header>

    @yield('conteudo')


    <div class="container">
        <footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
            <p class="col-md-4 mb-0">&copy;Prego Digital 2023</p>

            <a href="/" class="col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto link-dark text-decoration-none">
                <svg class="bi me-2" width="40" height="32">
                    <use xlink:href="#bootstrap"></use>
                </svg>
            </a>

            <ul class="nav col-md-4 justify-content-end">
                <li class="nav-item"><a href="{{route('home')}}" class="nav-link px-2">Home</a></li>
                <li class="nav-item"><a href="{{route('sobre')}}" class="nav-link px-2">Sobre Nós</a></li>
                <li class="nav-item"><a href="{{route('contate')}}" class="nav-link px-2">Contate Nos</a></li>
            </ul>
        </footer>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-OERcA2EqjJCMA+/3y+gxIOqMEjwtxJY7qPCqsdltbNJuaOe923+mo//f6V8Qbsw3" crossorigin="anonymous"></script>
</body>

</html>
