@extends('layouts.login')

@section('titulo', 'login')

@section('conteudo')

<div class="card fat">
    <div class="card-body">
        <h4 class="card-title">Login</h4>
        <form method="POST" class="my-login-validation" novalidate="">
            <div class="form-group">
                <label for="email">E-Mail</label>
                <input id="email" type="email" class="form-control" name="email" value="" required autofocus>
                <div class="invalid-feedback">
                    Email inválido
                </div>
            </div>

            <div class="form-group">
                <label for="password">Senha
                    <a href="esqueceu.php" class="float-right">
                        Esqueceu a Senha?
                    </a>
                </label>
                <input id="password" type="password" class="form-control" name="password" required data-eye>
                <div class="invalid-feedback">
                    Senha requerida
                </div>
            </div>

            <div class="form-group">
                <div class="custom-checkbox custom-control">
                    <input type="checkbox" name="remember" id="remember" class="custom-control-input">
                    <label for="remember" class="custom-control-label">Lembre-me</label>
                </div>
            </div>

            <div class="form-group m-0">
                <button type="submit" style="background-color:  #836FFF;" class="btn btn-primary btn-block">
                    Login
                </button>
            </div>
            <div class="mt-4 text-center">
                Nao tem uma conta? <a href="{{route('cadastrar')}}">Crie uma</a>
            </div>
        </form>
    </div>
</div>
<div class="footer">
    Copyright &copy; 2022 &mdash; Prego Digital
</div>
</div>
</div>
</div>

@endsection
