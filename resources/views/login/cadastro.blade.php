@extends('layouts.site')

@section('titulo', 'cadastrar')

@section('conteudo')

<div class="container meu-cadastro">
    <br>
    <div class="row justify-content-md-center">
        <div class="card-wrapper">
            <div class="brand mt-5">
                <a href="index.php">
                    <img src="/img/logoPD.png" alt="logo" />
                </a>
            </div>


            <div class="card fat">
						<div class="card-body">
                            @if ($errors->any())

@foreach ($errors->all() as $error)

<div>
    {{ $error }}
</div>

@endforeach

@endif
							<h4 class="card-title"></h4>
							<form method="POST" action="{{route('salvar')}}" class="my-login-validation" novalidate="">
                                @csrf
								<div class="form-group">
									<label for="name">Nome</label>
									<input id="name" type="text" class="form-control" placeholder="Seu nome" name="nome" required autofocus />
									<div class="invalid-feedback">Qual é o seu nome?</div>
								</div>

								<div class="form-group">
									<label for="cpf">CPF</label>
									<input id="cpf" type="text" class="form-control" placeholder="000.000.000-00" name="cpf" required data-eye />
									<div class="invalid-feedback">CPF invalido!</div>
								</div>

								<div class="form-group">
									<label for="email">E-Mail</label>
									<input id="email" type="email" class="form-control" placeholder="nome@example.com" name="email" required />
									<div class="invalid-feedback">E-mail Invalido!</div>
								</div>

								<div class="form-group">
									<label for="password">Senha</label>
									<input id="password" type="password" class="form-control" placeholder="********" name="password" require data-eye />
									<div class="invalid-feedback">Senha Requirida</div>
								</div>

								<div class="form-group">
									<label for="rua">Rua</label>
									<input id="rua" type="text" class="form-control" placeholder="Digite o nome da rua" name="rua" required data-eye />
								</div>

								<div class="form-group">
									<label for="numero">Numero</label>
									<input id="numero" type="text" class="form-control" placeholder="ex: 888" name="numero" required data-eye />
								</div>

								<div class="form-group">
									<label for="complemento">Complementos</label>
									<input id="complemento" type="text" class="form-control" placeholder="" name="complemento" required data-eye />
								</div>

								<div class="form-group">
									<label for="bairro">Bairro</label>
									<input id="bairro" type="text" class="form-control" placeholder="Digite o seu bairro" name="bairro" required data-eye />
								</div>

								<div class="form-group">
									<label for="cidade">Cidade</label>
									<input id="cidade" type="text" class="form-control" placeholder="Digite o nome da sua cidade" name="cidade" required data-eye />
								</div>

								<div class="form-group">
									<label for="inputState" >Estado</label>
									<select id="inputState" class="form-select" name="estado">
										<option selected>SP</option>
									</select>
								</div>

								<div class="form-group">
									<label for="cep">CEP</label>
									<input id="cep" type="text" class="form-control" placeholder="00000-000" name="cep" required data-eye />
									<div class="invalid-feedback">CEP Invalido!</div>
								</div>

								<div class="form-group">
									<label for="telefone">Telefone</label>
									<input id="telefone" type="text" class="form-control" placeholder="(00) 00000-0000" name="telefone" required data-eye />
								</div>

                                <div class="form-group">
									<label for="celular">Celular</label>
									<input id="celular" type="text" class="form-control" placeholder="(00) 00000-0000" name="celular" required data-eye />
								</div>

                                <br><br>

								<div class="form-group m-0">
									<button type="submit" style="background-color:  #836FFF;" class="btn btn-primary btn-block">
										Cadastrar
									</button>
								</div>
								<div class="mt-4 text-center">
									Já tem uma conta? <a href="login.php">Login</a>
								</div>
							</form>
						</div>
					</div>
					<div class="footer">Copyright &copy; 2023 | Prego Digital</div>
				</div>
			</div>
		</div>

@endsection
