@csrf



@if ($errors->any())

    @foreach ($errors->all() as $error)
        <div>
            {{ $error }}
        </div>
    @endforeach

@endif




<div class="col-md-12">
    <label for="titulo" class="form-label">Titulo</label>
    <input type="text" name="titulo" class="form-control @error('titulo') is-invalid @enderror" id="titulo"
        placeholder="" value="{{ old('valor', $compra->titulo) }}">


    @error('titulo')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>


<div class="col-md-12">
    <label for="descricao" class="form-label">Descrição</label>
    <input type="text" name="descricao" class="form-control @error('descricao') is-invalid @enderror" id="descricao"
        placeholder="" value="{{ old('valor', $compra->descricao) }}">


    @error('descricao')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>

<div class="col-md-12">
    <label for="valor" class="form-label">Valor Produto</label>
    <input type="text" name="valor" class="form-control @error('valor') is-invalid @enderror" id="valor"
        placeholder="" value="{{ old('valor', $compra->valor) }}">


    @error('valor')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>



<div class="col-md-12">
    <label for="datacompra" class="form-label">Data Compra</label>
    <input type="date" name="datacompra" class="form-control @error('datacompra') is-invalid @enderror" id="datacompra"
        placeholder="" value="{{ old('datacompra', $compra->datacompra) }}">


    @error('datacompra')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>




<div class="form-group">
    <label for="valordevia">Quanto Deve</label>
    <input id="valordevia" type="text" class="form-control @error('valordevia') is-invalid @enderror" placeholder="" value="{{ old('nome', $compra->valordevia) }}" name="valordevia" required data-eye />
    <div class="invalid-feedback">valordevia invalido!</div>
</div>

<div class="form-group">
    <label for="valorpago">Valor Pago</label>
    <input id="valorpago" type="text" class="form-control @error('valorpago') is-invalid @enderror" placeholder="" value="{{ old('nome', $compra->valorpago) }}" name="valorpago" required data-eye />
    <div class="invalid-feedback">valorpago invalido!</div>
</div>

<div class="form-group">
    <label for="quantofalta">Valor Pendente</label>
    <input id="quantofalta" type="text" class="form-control @error('quantofalta') is-invalid @enderror" placeholder="" value="{{ old('nome', $compra->quantofalta) }}" name="quantofalta" required data-eye />
    <div class="invalid-feedback">Quanto falta invalido!</div>
</div>




        <div class="col-12">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
