@csrf



@if ($errors->any())

    @foreach ($errors->all() as $error)
        <div>
            {{ $error }}
        </div>
    @endforeach

@endif

<div class="col-md-12">
    <label for="nome" class="form-label">Nome</label>
    <input type="text" class="form-control @error('nome') is-invalid @enderror" name="nome" id="nome"
        placeholder="Insira o Nome" value="{{ old('nome', $usuario->nome) }}">


    @error('nome')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>
<div class="col-md-12">
    <label for="email" class="form-label">E-mail</label>
    <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email"
        placeholder="Insira a E-mail" value="{{ old('email', $usuario->email) }}">


    @error('email')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>



<div class="col-md-12">
    <label for="senha" class="form-label">Senha</label>
    <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="senha"
        placeholder="">


    @error('password')
        <div class="invalid-feedback">
            {{ $message }}
        </div>
    @enderror


</div>


<div class="form-group">
    <label for="cpf">CPF</label>
    <input id="cpf" type="text" class="form-control @error('cpf') is-invalid @enderror" placeholder="000.000.000-00" value="{{ old('nome', $usuario->cpf) }}" name="cpf" required data-eye />
    <div class="invalid-feedback">CPF invalido!</div>
</div>

<div class="form-group">
    <label for="rua">Rua</label>
    <input id="rua" type="text" class="form-control @error('rua') is-invalid @enderror" placeholder="Digite o nome da rua" value="{{ old('nome', $usuario->rua) }}" name="rua" required data-eye />
</div>

<div class="form-group">
    <label for="numero">Numero</label>
    <input id="numero" type="text" class="form-control @error('numero') is-invalid @enderror" placeholder="ex: 888" value="{{ old('nome', $usuario->numero) }}" name="numero" required data-eye />
</div>

<div class="form-group">
    <label for="bairro">Bairro</label>
    <input id="bairro" type="text" class="form-control @error('bairro') is-invalid @enderror" placeholder="Digite o seu bairro" value="{{ old('nome', $usuario->bairro) }}" name="bairro" required data-eye />
</div>

<div class="form-group">
    <label for="cidade">Cidade</label>
    <input id="cidade" type="text" class="form-control @error('cidade') is-invalid @enderror" placeholder="Digite o nome da sua cidade" value="{{ old('nome', $usuario->cidade) }}" name="cidade" required data-eye />
</div>

<div class="form-group">
    <label for="inputState" >Estado</label>
    <select id="inputState" class="form-select" name="estado">
        <option selected>SP</option>
    </select>
</div>

<div class="form-group">
    <label for="cep">CEP</label>
    <input id="cep" type="text" class="form-control @error('cep') is-invalid @enderror" placeholder="00000-000" value="{{ old('nome', $usuario->cep) }}" name="cep" required data-eye />
    <div class="invalid-feedback">CEP Invalido!</div>
</div>

<div class="form-group">
    <label for="tipo">Tipo</label>
    <input id="tipo" type="text" class="form-control @error('tipo') is-invalid @enderror" placeholder="00000-000" value="{{ old('nome', $usuario->tipo) }}" name="tipo" required data-eye />
</div>

<div class="form-group">
    <label for="telefone">Telefone</label>
    <input id="telefone" type="text" class="form-control @error('telefone') is-invalid @enderror" placeholder="(00) 00000-0000" value="{{ old('nome', $usuario->telefone) }}" name="telefone" required data-eye />
</div>

<div class="form-group">
    <label for="celular">Celular</label>
    <input id="celular" type="text" class="form-control @error('celular') is-invalid @enderror" placeholder="(00) 00000-0000" value="{{ old('nome', $usuario->celular) }}" name="celular" required data-eye />
</div>



        <div class="col-12">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
